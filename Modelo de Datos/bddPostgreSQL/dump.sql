--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7 (Debian 11.7-0+deb10u1)
-- Dumped by pg_dump version 11.7 (Debian 11.7-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Categoria; Type: TABLE; Schema: public; Owner: facu
--

CREATE TABLE public."Categoria" (
    "id-categoria" integer NOT NULL,
    "nombre-categoria" text NOT NULL,
    descripcion text
);


ALTER TABLE public."Categoria" OWNER TO facu;

--
-- Name: Compra; Type: TABLE; Schema: public; Owner: facu
--

CREATE TABLE public."Compra" (
    "id-compra" integer NOT NULL,
    "monto-total" double precision NOT NULL,
    "fecha-compra" date NOT NULL
);


ALTER TABLE public."Compra" OWNER TO facu;

--
-- Name: Compra-Prenda; Type: TABLE; Schema: public; Owner: facu
--

CREATE TABLE public."Compra-Prenda" (
    "id-compra" integer NOT NULL,
    "id-prenda" integer NOT NULL,
    "cantidad-compra" integer NOT NULL,
    "precio-compra" double precision NOT NULL
);


ALTER TABLE public."Compra-Prenda" OWNER TO facu;

--
-- Name: Compra-Prenda-Promociones; Type: TABLE; Schema: public; Owner: facu
--

CREATE TABLE public."Compra-Prenda-Promociones" (
    "id-prenda" integer NOT NULL,
    "id-compra" integer NOT NULL,
    "nro-promocion" integer NOT NULL
);


ALTER TABLE public."Compra-Prenda-Promociones" OWNER TO facu;

--
-- Name: Precio; Type: TABLE; Schema: public; Owner: facu
--

CREATE TABLE public."Precio" (
    "fecha-precio" date NOT NULL,
    monto double precision NOT NULL,
    descripcion text,
    "id-prenda" integer NOT NULL
);


ALTER TABLE public."Precio" OWNER TO facu;

--
-- Name: Prenda; Type: TABLE; Schema: public; Owner: facu
--

CREATE TABLE public."Prenda" (
    "id-prenda" integer NOT NULL,
    "cantidad-total" integer NOT NULL,
    "id-categoria" integer NOT NULL
);


ALTER TABLE public."Prenda" OWNER TO facu;

--
-- Name: Prenda-Promocion; Type: TABLE; Schema: public; Owner: facu
--

CREATE TABLE public."Prenda-Promocion" (
    "id-prenda" integer NOT NULL,
    "nro-promocion" integer NOT NULL,
    cantidad integer NOT NULL,
    "porcentaje-descuento" integer NOT NULL,
    "precio-promocion" double precision NOT NULL
);


ALTER TABLE public."Prenda-Promocion" OWNER TO facu;

--
-- Name: Prenda-Promocion_id-prenda_seq; Type: SEQUENCE; Schema: public; Owner: facu
--

CREATE SEQUENCE public."Prenda-Promocion_id-prenda_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Prenda-Promocion_id-prenda_seq" OWNER TO facu;

--
-- Name: Prenda-Promocion_id-prenda_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: facu
--

ALTER SEQUENCE public."Prenda-Promocion_id-prenda_seq" OWNED BY public."Prenda-Promocion"."id-prenda";


--
-- Name: Prenda-Promocion_nro-promocion_seq; Type: SEQUENCE; Schema: public; Owner: facu
--

CREATE SEQUENCE public."Prenda-Promocion_nro-promocion_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Prenda-Promocion_nro-promocion_seq" OWNER TO facu;

--
-- Name: Prenda-Promocion_nro-promocion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: facu
--

ALTER SEQUENCE public."Prenda-Promocion_nro-promocion_seq" OWNED BY public."Prenda-Promocion"."nro-promocion";


--
-- Name: Promocion ; Type: TABLE; Schema: public; Owner: facu
--

CREATE TABLE public."Promocion " (
    "fecha-promocion-inicio" date NOT NULL,
    "fecha-promocion-fin" date NOT NULL,
    "monto-total" double precision NOT NULL,
    "nro-promocion" integer NOT NULL
);


ALTER TABLE public."Promocion " OWNER TO facu;

--
-- Name: reposicion; Type: TABLE; Schema: public; Owner: facu
--

CREATE TABLE public.reposicion (
    "fecha-reposicion" date NOT NULL,
    cantidad integer NOT NULL,
    descripcion text,
    "id-prenda" integer NOT NULL
);


ALTER TABLE public.reposicion OWNER TO facu;

--
-- Name: Prenda-Promocion id-prenda; Type: DEFAULT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Prenda-Promocion" ALTER COLUMN "id-prenda" SET DEFAULT nextval('public."Prenda-Promocion_id-prenda_seq"'::regclass);


--
-- Name: Prenda-Promocion nro-promocion; Type: DEFAULT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Prenda-Promocion" ALTER COLUMN "nro-promocion" SET DEFAULT nextval('public."Prenda-Promocion_nro-promocion_seq"'::regclass);


--
-- Name: Categoria Categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Categoria"
    ADD CONSTRAINT "Categoria_pkey" PRIMARY KEY ("id-categoria");


--
-- Name: Compra-Prenda-Promociones Compra-Prenda-Promociones_pkey; Type: CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Compra-Prenda-Promociones"
    ADD CONSTRAINT "Compra-Prenda-Promociones_pkey" PRIMARY KEY ("id-prenda", "id-compra", "nro-promocion");


--
-- Name: Compra-Prenda Compra-Prenda_pkey; Type: CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Compra-Prenda"
    ADD CONSTRAINT "Compra-Prenda_pkey" PRIMARY KEY ("id-compra", "id-prenda");


--
-- Name: Compra Compra_pkey; Type: CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Compra"
    ADD CONSTRAINT "Compra_pkey" PRIMARY KEY ("id-compra");


--
-- Name: Precio Precio_pkey; Type: CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Precio"
    ADD CONSTRAINT "Precio_pkey" PRIMARY KEY ("id-prenda", "fecha-precio");


--
-- Name: Prenda-Promocion Prenda-Promocion_pkey; Type: CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Prenda-Promocion"
    ADD CONSTRAINT "Prenda-Promocion_pkey" PRIMARY KEY ("id-prenda", "nro-promocion");


--
-- Name: Prenda Prenda_pkey; Type: CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Prenda"
    ADD CONSTRAINT "Prenda_pkey" PRIMARY KEY ("id-prenda");


--
-- Name: Promocion  Promocion _pkey; Type: CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Promocion "
    ADD CONSTRAINT "Promocion _pkey" PRIMARY KEY ("nro-promocion");


--
-- Name: reposicion reposicion_pkey; Type: CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public.reposicion
    ADD CONSTRAINT reposicion_pkey PRIMARY KEY ("fecha-reposicion", "id-prenda");


--
-- Name: Compra-Prenda-Promociones Compra-Prenda-Promociones_id-compra_fkey; Type: FK CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Compra-Prenda-Promociones"
    ADD CONSTRAINT "Compra-Prenda-Promociones_id-compra_fkey" FOREIGN KEY ("id-compra") REFERENCES public."Compra"("id-compra");


--
-- Name: Compra-Prenda-Promociones Compra-Prenda-Promociones_id-prenda_fkey; Type: FK CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Compra-Prenda-Promociones"
    ADD CONSTRAINT "Compra-Prenda-Promociones_id-prenda_fkey" FOREIGN KEY ("id-prenda", "nro-promocion") REFERENCES public."Prenda-Promocion"("id-prenda", "nro-promocion");


--
-- Name: Compra-Prenda Compra-Prenda_id-compra_fkey; Type: FK CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Compra-Prenda"
    ADD CONSTRAINT "Compra-Prenda_id-compra_fkey" FOREIGN KEY ("id-compra") REFERENCES public."Compra"("id-compra");


--
-- Name: Compra-Prenda Compra-Prenda_id-prenda_fkey; Type: FK CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Compra-Prenda"
    ADD CONSTRAINT "Compra-Prenda_id-prenda_fkey" FOREIGN KEY ("id-prenda") REFERENCES public."Prenda"("id-prenda");


--
-- Name: Precio Precio_id-prenda_fkey; Type: FK CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Precio"
    ADD CONSTRAINT "Precio_id-prenda_fkey" FOREIGN KEY ("id-prenda") REFERENCES public."Prenda"("id-prenda");


--
-- Name: Prenda-Promocion Prenda-Promocion_id-prenda_fkey; Type: FK CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Prenda-Promocion"
    ADD CONSTRAINT "Prenda-Promocion_id-prenda_fkey" FOREIGN KEY ("id-prenda") REFERENCES public."Prenda"("id-prenda");


--
-- Name: Prenda-Promocion Prenda-Promocion_nro-promocion_fkey; Type: FK CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Prenda-Promocion"
    ADD CONSTRAINT "Prenda-Promocion_nro-promocion_fkey" FOREIGN KEY ("nro-promocion") REFERENCES public."Promocion "("nro-promocion");


--
-- Name: Prenda Prenda_id-categoria_fkey; Type: FK CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public."Prenda"
    ADD CONSTRAINT "Prenda_id-categoria_fkey" FOREIGN KEY ("id-categoria") REFERENCES public."Categoria"("id-categoria") ON UPDATE SET DEFAULT ON DELETE SET DEFAULT;


--
-- Name: reposicion reposicion_id-prenda_fkey; Type: FK CONSTRAINT; Schema: public; Owner: facu
--

ALTER TABLE ONLY public.reposicion
    ADD CONSTRAINT "reposicion_id-prenda_fkey" FOREIGN KEY ("id-prenda") REFERENCES public."Prenda"("id-prenda");


--
-- PostgreSQL database dump complete
--

